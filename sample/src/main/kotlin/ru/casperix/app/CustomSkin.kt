package ru.casperix.app

import ru.casperix.light_ui.custom.SkinBuilder
import ru.casperix.light_ui.skin.Skin
import ru.casperix.math.vector.float32.Vector2f

class CustomSkin : Skin by SkinBuilder() {
    private val base = SkinBuilder()

    override val pushButton = base.pushButton.copy(defaultSize = Vector2f(100f, 40f))
    override val toggleButton = base.toggleButton.copy(defaultSize = Vector2f(100f, 40f))
    override val window = base.window.copy(titleHeight = 40f)
}