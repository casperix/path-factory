package ru.casperix.app.renderer

import ru.casperix.math.color.Colors
import ru.casperix.math.color.float32.Color3f
import ru.casperix.math.color.float32.Color4f

object RenderConfig {
    val LAND_LOCKED = Color3f(0.5f, 0.0f, 0.0f)
    val LAND_HARD = Color3f(0.5f, 0.5f, 0.0f)
    val LAND_EASY = Color3f(0.0f, 0.5f, 0.0f)

    val ROAD_THICK = 0.3f
    val ROAD_COLOR_1 = Color3f(0.9f, 0.9f, 1.0f)
    val ROAD_COLOR_2 = Color3f(0.7f, 0.7f, 1.0f)

    val AFFECTED_TILE_COLOR = Color4f(0.3f, 0.3f, 0.3f, 0.5f)
    val ORIGINAL_TILE_COLOR =Color4f(0.4f, 0.4f, 0.4f, 0.5f)
    val ACTUAL_TILE_COLOR = Color4f(0.5f, 0.5f, 0.5f, 0.5f)
}