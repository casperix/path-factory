package ru.casperix.app.renderer

import ru.casperix.app.renderer.RenderConfig.LAND_EASY
import ru.casperix.app.renderer.RenderConfig.LAND_HARD
import ru.casperix.app.renderer.RenderConfig.LAND_LOCKED
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.color.Colors
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.toQuad
import ru.casperix.path.custom.float_path.SurfaceMap
import ru.casperix.path.custom.float_path.Tile
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.material.SimpleMaterial
import ru.casperix.renderer.material.Texture2D
import ru.casperix.renderer.material.TextureConfig
import ru.casperix.renderer.material.TextureFilter
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.renderer.vector.vertex.Vertex
import ru.casperix.renderer.vector.vertex.VertexPosition2
import ru.casperix.renderer.vector.vertex.VertexTextureCoord

class SurfaceRenderer(val map: SurfaceMap) {
    val texture: Texture2D

    init {
        val pixelMap = PixelMap.RGBA(map.dimension.toDimension2i())
        val raw = pixelMap.RGBA()!!
        (0 until map.sizeX).forEach { x ->
            (0 until map.sizeY).forEach { y ->
                val tile = map.get(x, y)


                val color = if (!tile.complexity.isFinite()) {
                    LAND_LOCKED
                } else if (tile.complexity <= Tile.ROCK.complexity / 2f) {
                    LAND_EASY
                } else {
                    LAND_HARD
                }

                raw.set(x, y, color.toColor4b())
            }
        }
        texture = Texture2D(pixelMap, TextureConfig(TextureFilter.LINEAR, TextureFilter.NEAREST))
    }


    fun render(renderer: Renderer2D) {
        renderer.drawQuad(SimpleMaterial(texture), Box2f.byDimension(Vector2f.ZERO, map.dimension.toVector2f()).toQuad().convert {
            Vertex(VertexPosition2(it), VertexTextureCoord(it.sign))
        }, Matrix3f.IDENTITY)
    }
}