package ru.casperix.app

import ru.casperix.demo_platform.application.DemoApplication
import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.opengl.core.app.jvmSurfaceLauncher
import kotlin.random.Random


fun main() {
    jvmSurfaceLauncher(true) {
        SkinProvider.skin = CustomSkin()
        DemoApplication(it, GirdPathDemo(Random(1)))
    }
}