package ru.casperix.path

import ru.casperix.math.array.CustomMap2D
import ru.casperix.math.array.Map2D
import ru.casperix.math.array.MutableMap2D
import ru.casperix.math.random.nextVector2i
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.path.api.Path
import ru.casperix.path.custom.PathFactory
import ru.casperix.path.util.drawPathInfo
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class PathFactoryTest {
	@Test
	fun emptyMap() {
		val map = CustomMap2D.create(Vector2i(32, 32)) { 0 }

		val start = Vector2i(16, 1)
		val finish = Vector2i(16, 30)

		val info = getPathOnMap(map, start, finish)
		val path = validatePath(info.path, start, finish)

		assertEquals(30, path.nodes.size)
		assertEquals(58, path.weight)

		drawPathInfo(info, path, { it }, { map.isOutside(it) || map.get(it) != 0 })
	}


	@Test
	fun randomMap() {
		val map = createRandomMap()

		val start = Vector2i(2, 2)
		val finish = Vector2i(30, 30)
		map.set(start, 0)
		map.set(finish, 0)

		val info = getPathOnMap(map, start, finish)
		val path = validatePath(info.path, start, finish)

		drawPathInfo(info, path, { it }, { map.isOutside(it) || map.get(it) != 0 })
	}

	@Test
	fun benchmark() {
		val map = createRandomMap()
		val random = Random(0)

		for (p in 1..100000) {
			val start = random.nextVector2i(Vector2i(2), Vector2i(60))
			val finish =  random.nextVector2i(Vector2i(2), Vector2i(60))
			map.set(start, 0)
			map.set(finish, 0)

			getPathOnMap(map, start, finish)
		}
	}

	@Test
	fun hardMap() {
		val map = createHardMap()

		val start = Vector2i(50, 46)
		val finish = Vector2i(50, 54)

		val info = getPathOnMap(map, start, finish)
		val path = validatePath(info.path, start, finish)

		drawPathInfo(info, path, { it }, { map.isOutside(it) || map.get(it) != 0 })
	}

	@Test
	fun checkInverse() {
		val map = createHardMap()

		val start = Vector2i(50, 46)
		val finish = Vector2i(50, 54)

		val info1 = getPathOnMap(map, start, finish)
		val info2 = getPathOnMap(map, finish, start)
		val path1 = validatePath(info1.path, start, finish)
		val path2 = validatePath(info2.path, finish, start)

		assertTrue { info1.weight == info2.weight }
		assertTrue { path1.weight == path2.weight }
		assertTrue { path1.nodes.size == path2.nodes.size }
	}

	private fun createRandomMap(): MutableMap2D<Int> {
		val random = Random(0)
		return CustomMap2D.create(Vector2i(64, 64)) { if (random.nextDouble() > 0.9) 1 else 0 }
	}

	private fun createHardMap():MutableMap2D<Int> {
		val map = CustomMap2D.create(Vector2i(100, 100)) { 0 }

		for (x in 0..99) {
			map.set(x, 48, 1)
			map.set(x, 52, 1)
		}
		map.set(35, 48, 0)
		map.set(65, 52, 0)


		for (y in 0..99) {
			map.set(48, y, 1)
			map.set(52, y, 1)
		}
		map.set(48, 35, 0)
		map.set(52, 65, 0)

		map.set(48, 49, 0)
		map.set(52, 51, 0)
		return map
	}


	private fun validatePath(path: Path<Vector2i>?, start: Vector2i, finish: Vector2i): Path<Vector2i> {
		assertNotNull(path)
		assertEquals(path.start, start)
		assertEquals(path.finish, finish)
		return path
	}


	private fun getPathOnMap(map: Map2D<Int>, start: Vector2i, finish: Vector2i): PathFactory<Vector2i> {
		val estimator = Map2DPathEstimator(map, finish)
		return PathFactory(estimator, start)
	}
}
