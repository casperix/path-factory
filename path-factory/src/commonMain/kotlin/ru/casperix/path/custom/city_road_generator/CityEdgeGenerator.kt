package ru.casperix.path.custom.city_road_generator

import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.random.nextDegreeFloat
import ru.casperix.math.random.nextFloat
import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.misc.randomWeighted
import kotlin.math.max
import kotlin.random.Random

/**
 *  Basic city-street generator.
 *  Recommended extend this.
 */
class CityEdgeGenerator(val config: CityMeshConfig) : EdgeGenerator<CityRoad> {

    override fun getAngle(random: Random, builder: MeshBuilder<CityRoad>, point: Point<CityRoad>): DegreeFloat {
        val baseLevel = point.marker.level
        val levelVariants = listOf(
            Pair(baseLevel, 16.0),
            Pair(max(0, baseLevel - 1), 4.0),
            Pair(max(0, baseLevel - 2), 1.0),
        )
        val nextLevel = levelVariants.randomWeighted(random)

        val hasLevelChanged = nextLevel != baseLevel

        val angleVariants =
            if (hasLevelChanged) {
                listOf(
                    Pair(DegreeFloat(180f), 1.0),
                    Pair(DegreeFloat(-90f), 8.0),
                    Pair(DegreeFloat(90f), 8.0),
                )
            } else {
                listOf(
                    Pair(DegreeFloat(-90f), 1.0),
                    Pair(DegreeFloat(90f), 1.0),
                    Pair(DegreeFloat(180f), 64.0),
                )
            }

        val connections = point.connections.map {
            Edge(LineSegment2f(point.position, it))
        }

        val equalLevelConnections = connections.filter { edge ->
            val level = builder.edges[edge]?.level
            level == nextLevel
        }

        val baseAngle = if (equalLevelConnections.isNotEmpty()) {
            equalLevelConnections.random(random).direction
        } else if (connections.isNotEmpty()) {
            connections.random(random).direction
        } else {
            random.nextDegreeFloat()
        }

        val deltaAngle = if (random.nextFloat() > 0.9f) {
            random.nextDegreeFloat(-config.deltaAngle, config.deltaAngle)
        } else {
            DegreeFloat.ZERO
        }

        return (baseAngle + angleVariants.randomWeighted(random) + deltaAngle).normalize()
    }

    override fun getDist(random: Random, point: Point<CityRoad>): Float {
        val randomDist = if (random.nextFloat() > 0.9f) random.nextFloat(-config.deltaDist, config.deltaDist) else 0f
        val gradientDistFactor = 1f//if (point.position.y < 0f) 2f else 1f
        return gradientDistFactor * (config.basisDist + randomDist)
    }
}