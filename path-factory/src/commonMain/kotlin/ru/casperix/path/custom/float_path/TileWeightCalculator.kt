package ru.casperix.path.custom.float_path

import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.int32.Vector2i

object TileWeightCalculator {
    fun getEdgeComplexity(mapInfo: MapInfo, segment: LineSegment2f): Float {
        val startTile = getPointComplexity(mapInfo, segment.start)
        val middleTile = getPointComplexity(mapInfo,segment.median())
        val finishTile = getPointComplexity(mapInfo,segment.finish)

        return (startTile + middleTile + finishTile) / 3f * segment.length()
    }

    fun getTileComplexity(mapInfo: MapInfo, position: Vector2i): Float {
        return if (mapInfo.roads.contains(position)) {
            1f
        } else {
            return mapInfo.map.get(position).complexity
        }
    }

    fun getPointComplexity(mapInfo: MapInfo, position: Vector2f): Float {
        val range = 0.4f
        return listOf(
            position + Vector2f(-range, -range),
            position + Vector2f(range, -range),
            position + Vector2f(range, range),
            position + Vector2f(-range, range),
        ).maxOf {
            getTileComplexity(mapInfo, it.toVector2i())
        }
    }

    fun getWorstTile(tileList: Collection<Tile>): Tile {
        return if (tileList.contains(Tile.WATER)) {
            Tile.WATER
        } else if (tileList.contains(Tile.ROCK)) {
            Tile.ROCK
        } else {
            Tile.GRASS
        }
    }
}