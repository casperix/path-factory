package ru.casperix.path.custom.float_path

import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.path.custom.PathFactory

class FloatPathFactory(val mapInfo: MapInfo, val neighboursMode: NeighboursMode, val maxTryAmount:Int) {
    fun generatePath(A: Vector2i, B: Vector2i): List<FloatPath> {
        val factory = PathFactory(GirdBasedPath(mapInfo, neighboursMode, A, B), A)

        val basePath = factory.path?.nodes.orEmpty()
        val basePathGroupList = splitByEasyPoints(basePath, mapInfo.roads)

        return basePathGroupList.map { basePathGroup ->
            val actualPath = tight(basePathGroup, maxTryAmount) {
                TileWeightCalculator.getEdgeComplexity(mapInfo, it)
            }.toMutableList()

            val nearStart = mapInfo.nearRoad(actualPath.first())
            if (nearStart != null) {
                actualPath[0] = nearStart
            }

            val nearFinish = mapInfo.nearRoad(actualPath.last())
            if (nearFinish != null) {
                actualPath[actualPath.lastIndex] = nearFinish
            }

            FloatPath(factory.searchMap.keys.toList(), basePathGroup, actualPath).apply {
                mapInfo.roadMap += pathTiles
            }
        }

    }

    private fun tight(source: List<Vector2i>, maxTryAmount:Int, edgeWeight: (LineSegment2f) -> Float): List<Vector2f> {
        val base = source.map { it.toVector2f() + Vector2f(0.5f) }.toMutableList()

        var changed = true
        var tryRemains = maxTryAmount
        while (changed && tryRemains-- > 0) {
            changed = false

            base.forEachIndexed { index, current ->
                if (index == 0 || index == base.lastIndex) {

                } else {
                    val prev = base[index - 1]
                    val next = base[index + 1]


                    val baseWeight = edgeWeight(LineSegment2f(prev, current)) + edgeWeight(LineSegment2f(current, next))

                    val middle = (prev + current + next) / 3f

                    val middleWeight = edgeWeight(LineSegment2f(prev, middle)) + edgeWeight(LineSegment2f(middle, next))

                    if (middleWeight < baseWeight) {
                        base[index] = middle
                        changed = changed || (middleWeight / baseWeight) < 0.99999f
                    }
                }
            }
        }

        return base
    }

    private fun splitByEasyPoints(points: List<Vector2i>, easyPoints: Set<Vector2i>): List<List<Vector2i>> {
        val output = mutableListOf<List<Vector2i>>()
        var accumulator = mutableListOf<Vector2i>()
        points.forEach {
            if (easyPoints.contains(it)) {
                if (accumulator.isNotEmpty()) {
                    output += accumulator
                    accumulator = mutableListOf()
                }
            } else {
                accumulator += it
            }
        }
        if (accumulator.isNotEmpty()) {
            output += accumulator
        }
        return output
    }

}