package ru.casperix.path.custom.float_path

import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.int32.Vector2i

class FloatPath(val affectedTiles: List<Vector2i>, val originalTiles: List<Vector2i>, val pathPoints: List<Vector2f>) {
    val pathTiles = pathPoints.associateBy { it.toVector2i() }
}