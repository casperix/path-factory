package ru.casperix.path.custom.city_road_generator

import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.collection.getLooped
import ru.casperix.math.geometry.float32.Geometry2Float
import ru.casperix.math.geometry.length
import ru.casperix.math.intersection.float32.Intersection2Float
import ru.casperix.math.straight_line.float32.LineSegment2f

class CityMeshValidator(val config: CityMeshConfig) : MeshChecker<CityRoad> {
    override fun canReplace(lastMarker: CityRoad, nextMarker: CityRoad): Boolean {
        return lastMarker.level < nextMarker.level
    }

    override fun isAcceptable(point: Point<CityRoad>): Boolean {
        if (point.connections.size <= 1) {
            return true
        }

        val sortedAngles = point.connections.map { target ->
            val segment = LineSegment2f(point.position, target)
            DegreeFloat.byDirection(segment.delta())
        }.sorted()

        sortedAngles.forEachIndexed { index, last ->
            val next = sortedAngles.getLooped(index + 1)


            if (DegreeFloat.betweenAngles(last, next) <= config.minAngle) {
                return false
            }
        }
        return true
    }

    override fun isAcceptable(edge: Edge): Boolean {
        if (edge.connection.length() < config.minLength) {
            return false
        }
        return true
    }

    override fun isAcceptable(edgeA: Edge, edgeB: Edge): Boolean {
        val a = edgeA.connection
        val b = edgeB.connection

        val aa = Box2f.byCorners(a.start, a.finish).grow(config.closestDist)
        val bb = Box2f.byCorners(b.start, b.finish).grow(config.closestDist)
        if (!Intersection2Float.hasBoxWithBox(aa, bb)) {
            return true
        }

        if (Intersection2Float.getSegmentWithSegmentWithoutEnd(a, b) != null) {
            return false
        }

        val points = setOf(b.start, b.finish, a.start, a.finish)

        if (points.size == 4) {
            val connection = Geometry2Float.closestConnectionBetweenSegments(b.toLine(), a.toLine())
            if (connection.length() < config.closestDist) {
                return false
            }
        }

        return true
    }
}