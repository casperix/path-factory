package ru.casperix.path.custom.float_path

enum class NeighboursMode {
    ORTHOGONAL,
    DIAGONAL,
    EXTENDED2,
    EXTENDED3,
}